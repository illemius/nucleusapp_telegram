from NucleusUtils.versions import Version

VERSION = Version(0, 2, 2)
__version__ = VERSION.version
