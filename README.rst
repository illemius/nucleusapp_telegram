# nucleusapp_telegram:
It's module for (NucleusApp)[https://pypi.python.org/pypi?name=NucleusApp&:action=display]
Used TeleSocket service and pyTelegramBotAPI library.

## Settings example
```python3
TELEGRAM = {
    "TOKEN": "TOKEN",
    "TELESOCKET_TOKEN": "TOKEN",
    "TELESOCKET_MANAGER": False,
    "SKIP_PENDING": True,
    "ROOT_UID": 1234567,
}
```
